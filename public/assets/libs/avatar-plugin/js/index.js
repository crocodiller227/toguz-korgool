// vars
var result = document.querySelector('.result'),
    img_result = document.querySelector('.img-result'),
    img_w = document.querySelector('.img-w'),
    img_h = document.querySelector('.img-h'),
    options = document.querySelector('.options'),
    save = document.querySelector('.save'),
    cropped = document.querySelector('.cropped'),
    dwn = document.querySelector('.download'),
    upload = document.querySelector('#file-input'),
    cropper = '',
    cutResult = $('.cut-result'),
    cut = $('.cut'),
    fullName = $('#fullNameInput'),
    fullNameValue = false;


cutResult.click(function () {
    upload.click();
});

fullName.on('change', function () {
    fullNameValue = true;
});


// on change show image with crop options
upload.addEventListener('change', function (e) {
    if (e.target.files.length) {
        // start file reader
        var reader = new FileReader();
        reader.onload = function (e) {
            if (e.target.result) {
                // create new image
                var img = document.createElement('img');
                img.id = 'image';
                img.src = e.target.result;
                // clean result before
                result.innerHTML = '';
                // append new image
                result.appendChild(img);
                // show save btn and options
                save.classList.remove('hide');
                cut.show();
                cutResult.hide();
                // init cropper
                cropper = new Cropper(img, {
                    aspectRatio: 200 / 200,
                    background: false,
                    autoCropArea: 1,
                });
            }
        };
        reader.readAsDataURL(e.target.files[0]);
    }
});

// save on click
save.addEventListener('click', function (e) {
    e.preventDefault();
    var imgSrc = null;
    if (cropper) {
        imgSrc = cropper.getCroppedCanvas({
            width: img_w.value, // input value
        }).toDataURL();
        cropped.classList.remove('hide');
        img_result.classList.remove('hide');
        cropped.src = imgSrc;
        dwn.classList.remove('hide');
        dwn.download = 'imageName.png';
        dwn.setAttribute('href', imgSrc);
        cut.hide();
        cutResult.show();
    }

    if (imgSrc || fullNameValue) {
        var url = "/user/edit";
        var user = {
            fullName : fullName.val(),
            avatar   : imgSrc,
        };
        $.post(url, user, onAjaxSuccess, ).fail(function () {});
        fullNameValue = false;
        cropper = '';
    }

    function onAjaxSuccess()
    {
        $('.notification').toggleClass('notification');
    }

});