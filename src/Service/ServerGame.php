<?php
/**
 * Created by PhpStorm.
 * User: kremnevea
 * Date: 07.09.18
 * Time: 7:24
 */

namespace App\Service;

use App\CustomLogger\CustomLogger;
use App\Handler\GameHandler;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServerGame implements MessageComponentInterface
{
    /** @var ContainerInterface $container */
    protected $container;
    /** @var GameHandler $handler */
    protected $handler;

    /**
     * ServerChat constructor.
     * @param ContainerInterface $container
     * @param GameHandler $handler
     */
    public function __construct(ContainerInterface $container, GameHandler $handler)
    {
        $this->container = $container;
        $this->handler = $handler;
    }

    /**
     * @param ConnectionInterface $conn
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     */
    public function onOpen(ConnectionInterface $conn)
    {
        parse_str($conn->httpRequest->getUri()->getQuery(), $queryParameters);
        $token = $queryParameters['token'];
        $keyRoom = $queryParameters['key'];
        $user = $this->handler->getUserByToken($token, $conn);

        if ($user && $this->handler->checkRoom($keyRoom,$user->getUsername())) {
            $this->handler->addToRoom($keyRoom, $user->getUsername(), $conn);
            return;
        }

        $this->handler->disconnectClient($conn);
    }


    /**
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $this->handler->messageProcessing($msg, $from);
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->handler->endGame($conn, false, true);
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->handler->endGame($conn, false, true);
        CustomLogger::_logNotice("An error has occurred: {$e->getMessage()}", self::class);
    }
}