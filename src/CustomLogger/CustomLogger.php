<?php

namespace App\CustomLogger;

use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomLogger
{
    static private $filename;
    static private $prefix = 'WEB';


    public function __construct(ContainerInterface $container)
    {
        self::$filename = $container->getParameter('logger_file_path');
    }

    static function _logWarning($message, $pathName = '', $prefix = null) {
        $data = "[" .self::getFormatCurrentDate() . "] ".($prefix ?? (self::$prefix))." || WARNING: {$message} || LogPlace: {$pathName}"."\n";
        file_put_contents(self::$filename, $data, FILE_APPEND | LOCK_EX);
    }

    static function _logNotice($message, $pathName = '', $prefix = null) {
        $data = "[" .self::getFormatCurrentDate() . "] ".($prefix ?? (self::$prefix))." || NOTICE: {$message} || LogPlace: {$pathName}"."\n";
        file_put_contents(self::$filename, $data, FILE_APPEND | LOCK_EX);
    }

    static function _logError($message, $pathName = '', $prefix = null) {
        $data = "[" .self::getFormatCurrentDate() . "] ".($prefix ?? (self::$prefix))." || ERROR: {$message} || LogPlace: {$pathName}"."\n";
        file_put_contents(self::$filename, $data, FILE_APPEND | LOCK_EX);
    }

    static function _logDebug($message, $pathName = '', $prefix = null) {
        $data = "[" .self::getFormatCurrentDate() . "] ".($prefix ?? (self::$prefix))." || DEBUG: {$message} || LogPlace: {$pathName}"."\n";
        file_put_contents(self::$filename, $data, FILE_APPEND | LOCK_EX);
    }

    static function getFormatCurrentDate() {
        $current_date = new \DateTime;
        return $current_date->format("d-m-Y H-i-s");
    }

    static function setPrefix($prefix) {
        self::$prefix = $prefix;
    }

}